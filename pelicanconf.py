#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# Site
AUTHOR = 'SL3 Team'
SITENAME = 'Supinfo Low-Level Laboratory'
SITEURL = 'http://lab-sl3.org'

# Language, Date format, Timezone...
TIMEZONE = 'America/Montreal'
DEFAULT_LANG = 'fr'

# Navigation
DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False

MENUITEMS = (('Accueil', '%s/pages/accueil.html' % SITEURL),
             ('Blog', SITEURL),
             ('Forum', '%s/forums/' % SITEURL),)

# Look
THEME = 'notmyidea-fr'

# Articles
DEFAULT_CATEGORY = 'news'
DEFAULT_PAGINATION = 5

ARTICLE_URL = 'articles/{category}/{slug}.html'
ARTICLE_SAVE_AS = 'articles/{category}/{slug}.html'

ARTICLE_LANG_URL = 'articles/{category}/{slug}-{lang}.html'
ARTICLE_LANG_SAVE_AS = 'articles/{category}/{slug}-{lang}.html'

# Blogroll
LINKS =  (('Pelican', 'http://docs.notmyidea.org/alexis/pelican/'),
          ('Python.org', 'http://python.org'),
          ('Jinja2', 'http://jinja.pocoo.org'),)
